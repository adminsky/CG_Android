package com.lmm.cg.zjgcg.pages.rnplugins;

import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.R;
import com.lmm.cg.zjgcg.config.AppKey;
import com.lmm.cg.zjgcg.config.AppKeyValueUtils;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RNPlugin(method = "printerCpgl")
public class PrinterPlugins extends BasePlugin<RNMainActivity> {


    RNMainActivity activity;


    public PrinterPlugins(RNMainActivity act) {
        super(act);
        activity = act;
    }

    @Override
    protected void doAction(Map<String, Object> map, final Callback callback) {

        activity.addCallBack("printerCpgl", callback);

        String carNo = "";
        if (map.containsKey("carNo")) {
            carNo = map.get("carNo").toString();
        }
        String TDBH = "";
        if (map.containsKey("TDBH")) {
            TDBH = map.get("TDBH").toString();
        }

        String TDDate = "";
        if (map.containsKey("TDDate")) {
            TDDate = map.get("TDDate").toString();
        }

        String TDAddress = "";
        if (map.containsKey("TDAddress")) {
            TDAddress = map.get("TDAddress").toString();
        }

        String CLPhone = "";
        if (map.containsKey("CLPhone")) {
            CLPhone = map.get("CLPhone").toString();
        }
        String BGDW = "";
        if (map.containsKey("BGDW")) {
            BGDW = map.get("BGDW").toString();
        }

        String CLDD = "";
        if (map.containsKey("CLDD")) {
            CLDD = map.get("CLDD").toString();
        }
        String JZDW = "";
        if (map.containsKey("JZDW")) {
            JZDW = map.get("JZDW").toString();
        }

        String deviceName = AppKeyValueUtils.getValue(AppKey.BlueToothName);
        if (deviceName != null && deviceName.length() > 0) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
            String message = activity.getResources().getString(R.string.PrintContent);

            message = message.replace("[标题]", "                违法停车提示单");
            message = message.replace("[贴单编号]", "                              NO:" + TDBH);
            message = message.replace("[车牌号码]", "   " + carNo + "  ");
            message = message.replace("[年月日时分]", "   时  间：" + TDDate);
            message = message.replace("[违章地点]", "   地  点：" + TDAddress);
            message = message.replace("[内容1]", "   已被拍摄并向" + BGDW + "报告，请您：");
            message = message.replace("[内容2]", "   在2日内携带有效证件到" + CLDD + "接受调查处理，如果您不方便到以上地址接受处理，可以在车辆审验时到张家港市公安局车辆管理所下辖的车辆检测点接受处理。");
            message = message.replace("[内容3]", "   处理时间：法定工作日内。联系电话：" + CLPhone);
            message = message.replace("[友情提示]", "                    友情提示");
            message = message.replace("[提示1]", "   1. 机动车所有人登记的住所地址或者联系电话发生变化时，请及时向登记车辆管理所申请变更备案。");
            message = message.replace("[提示2]", "   2. 临时停放机动车的驾驶人在驾驶室的，听从指挥立即驶离的，不进行拍摄。");
            message = message.replace("[提示3]", "   3. 在24小时内同一时间、同一地点违法停车的，可以认定为一次违法停车行为。");
            message = message.replace("[张家港市城市管理行政执法局(监制)]", "            " + JZDW + "(监制)");
            message = message.replace("[年月日]", "                              " + formatter.format(new Date()));
            message = message.replace("[空白]", "");

            activity.connectAndPrint(message, "");

        }
    }


}
