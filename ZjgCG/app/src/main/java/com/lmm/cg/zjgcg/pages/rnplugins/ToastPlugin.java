package com.lmm.cg.zjgcg.pages.rnplugins;

import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;
import com.lmm.dresswisdom.lmmframe.util.ToastUtils;


import java.util.Map;

/**
 * @author xmgong
 * @date 2017/11/15
 */
@RNPlugin(method = "showinfo")
public class ToastPlugin extends BasePlugin<RNMainActivity> {


    public ToastPlugin(RNMainActivity act) {
        super(act);
    }

    @Override
    protected void doAction(Map map, Callback callback) {
        if (map.containsKey("info")) {
            ToastUtils.showToastShort(map.get("info").toString());
        }
    }
}
