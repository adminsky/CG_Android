package com.lmm.cg.zjgcg.dbmodel;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by xmgong on 2016/12/14.
 */

public class AppKeyValueModel extends RealmObject {

    @PrimaryKey
    private String Key;

    private String Value;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }
}
