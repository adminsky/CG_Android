package com.lmm.cg.zjgcg.utils;

import com.lmm.dresswisdom.lmmframe.util.LogUtils;

import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by xmgong on 2017/8/31.
 */

public class RealmUtils {
    public static Realm getRealm() {
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("sywuser.realm")
                .schemaVersion(1)
                .build();
        return Realm.getInstance(config);
    }

    /**
     * 数据迁移操作
     */
    static class MyMigration implements RealmMigration {

        @Override
        public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
            RealmSchema schema = realm.getSchema();
            LogUtils.write("migration" + "Version:" + oldVersion);
//            if (oldVersion == 1) {
//                schema.create("UserInfoModel").addField("userid", String.class, FieldAttribute.PRIMARY_KEY)
//                        .addField("name", String.class);
//                oldVersion++;
//            }
//            if (oldVersion == 2) {
//                schema.get("UserInfoModel").addField("sexx", String.class);
//                oldVersion++;
//            }
        }
    }
}
