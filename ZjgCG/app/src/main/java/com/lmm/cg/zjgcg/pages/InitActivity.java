package com.lmm.cg.zjgcg.pages;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.igexin.sdk.PushManager;
import com.lmm.cg.zjgcg.R;
import com.lmm.cg.zjgcg.service.DemoIntentService;
import com.lmm.cg.zjgcg.service.DemoPushService;
import com.lmm.cg.zjgcg.service.UpdataService;
import com.lmm.dresswisdom.lmmframe.util.LogUtils;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import kr.co.namee.permissiongen.PermissionFail;
import kr.co.namee.permissiongen.PermissionGen;
import kr.co.namee.permissiongen.PermissionSuccess;

@Route(path = "/page/init")
public class InitActivity extends AppBaseActivity {

    public ScheduledExecutorService service = null;
    private int showTime = 3;
    private Handler mhandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            super.dispatchMessage(msg);
            if (msg.what == 1) {
                if (showTime > 0) {
                    showTime--;
                } else {
                    toMain();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        hideActionBar();
        service = new ScheduledThreadPoolExecutor(1);

        PermissionDialog();
        PushManager.getInstance().initialize(this.getApplicationContext(), DemoPushService.class);
        PushManager.getInstance().registerPushIntentService(this.getApplicationContext(), DemoIntentService.class);
    }


    private void PermissionDialog() {
        PermissionGen.needPermission(this, 100,
                new String[]{
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.CALL_PHONE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS,
                        Manifest.permission.WRITE_SETTINGS,
                        Manifest.permission.RECORD_AUDIO,
                        Manifest.permission.SYSTEM_ALERT_WINDOW});
    }

    @PermissionSuccess(requestCode = 100)
    public void doSomething() {
        LogUtils.write("PermissionSuccess");
        start();
        startUpload();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        PermissionGen.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @PermissionFail(requestCode = 100)
    public void permissFaile() {
        LogUtils.write("PermissionFail");
        start();
        startUpload();
    }

    /**
     * 开始更新数据
     */
    private void startUpload() {
        Intent uploadService = new Intent(this, UpdataService.class);
        startService(uploadService);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        service.shutdown();
    }

    private void start() {
        // 从现在开始1秒钟之后，每隔1秒钟执行一次job1
        service.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                mhandler.sendEmptyMessage(1);
            }
        }, 1, 1, TimeUnit.SECONDS);
    }

    private void toMain() {
//        ARouter.getInstance().build("/page/test").navigation();
        ARouter.getInstance().build("/page/main").navigation();
        finish();
    }


}
