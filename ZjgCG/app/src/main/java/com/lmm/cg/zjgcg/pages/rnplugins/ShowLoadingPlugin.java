package com.lmm.cg.zjgcg.pages.rnplugins;

import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;

import java.util.Map;

/**
 * @author xmgong
 * @date 2017/12/4
 */
@RNPlugin(method = "showloading")
public class ShowLoadingPlugin extends BasePlugin<RNMainActivity> {
    public ShowLoadingPlugin(RNMainActivity act) {
        super(act);
    }

    @Override
    protected void doAction(Map<String, Object> map, Callback callback) {
        activity.showDefaultDialog();
    }
}
