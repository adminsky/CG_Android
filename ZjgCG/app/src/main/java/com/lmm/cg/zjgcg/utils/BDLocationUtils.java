package com.lmm.cg.zjgcg.utils;

import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.geocode.GeoCodeOption;
import com.baidu.mapapi.search.geocode.GeoCodeResult;
import com.baidu.mapapi.search.geocode.GeoCoder;
import com.baidu.mapapi.search.geocode.OnGetGeoCoderResultListener;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;

/**
 * Created by xmgong on 2018/2/1.
 * 百度位置
 */

public class BDLocationUtils {

    GeoCoder mSearch = null;
    CallBackLocation callBackLocation = null;

    public BDLocationUtils(CallBackLocation call) {
        mSearch = GeoCoder.newInstance();
        mSearch.setOnGetGeoCodeResultListener(listener);
        this.callBackLocation = call;
    }

    public void searchLocation(String city, String address) {
        mSearch.geocode(new GeoCodeOption()
                .city(city)
                .address(address));
    }

    OnGetGeoCoderResultListener listener = new OnGetGeoCoderResultListener() {

        @Override
        public void onGetGeoCodeResult(GeoCodeResult result) {
            if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                //没有检索到结果
                callBackLocation.backLocation(null);
            }
            //获取地理编码结果
            callBackLocation.backLocation(result);
        }

        @Override
        public void onGetReverseGeoCodeResult(ReverseGeoCodeResult result) {
            if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
                //没有找到检索结果
            }
            //获取反向地理编码结果
        }
    };


    public void Destory() {
        mSearch.destroy();
    }


    public interface CallBackLocation {
        void backLocation(GeoCodeResult result);
    }
}
