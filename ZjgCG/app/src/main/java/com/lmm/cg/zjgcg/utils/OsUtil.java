package com.lmm.cg.zjgcg.utils;

import android.app.Activity;
import android.text.TextUtils;
import android.view.Window;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 作者： 周魏楠
 * 创建时间： 2018/4/18 17:06
 * 版本： [1.0, 2018/4/18]
 * 描述： 判断系统的工具类，设置系统状态栏工具类
 */
public class OsUtil {

    /**
     * 判断是不是小米
     *
     * @return
     */
    public static boolean isMIUI() {
        return !TextUtils.isEmpty(getSystemProperty("ro.miui.ui.version.name"));
    }

    /**
     * 判断是不是魅族
     *
     * @return
     */
    public static boolean isFlyme() {
        return getMeizuFlymeOSFlag().toLowerCase().contains("flyme");
    }

    private static String getSystemProperty(String propName) {
        String line;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop " + propName);
            input = new BufferedReader(new InputStreamReader(p.getInputStream()), 1024);
            line = input.readLine();
            input.close();
        } catch (IOException ex) {
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return line;
    }

    /**
     * 设置状态栏图标为深色和魅族特定的文字风格
     *
     * @param activity 需要设置的窗口
     * @param dark     是否把状态栏颜色设置为深色
     * @return boolean 成功执行返回true
     */
    public static void setFlymeStatusBarDarkIcon(Activity activity, boolean dark) {
        StatusbarColorUtils.setStatusBarDarkIcon(activity, dark);
    }

    /**
     * 设置小米状态栏图标为深色
     *
     * @param window
     * @param dark
     * @return
     */
    public static void setMiuiStatusBarDarkIcon(Window window, boolean dark) {
        try {
            Class clazz = window.getClass();
            Class layoutParams = Class.forName("android.view.MiuiWindowManager$LayoutParams");
            Field field = layoutParams.getField("EXTRA_FLAG_STATUS_BAR_DARK_MODE");
            int darkModeFlag = field.getInt(layoutParams);
            Method extraFlagField = clazz.getMethod("setExtraFlags", int.class, int.class);
            if (dark) {    //状态栏亮色且黑色字体
                extraFlagField.invoke(window, darkModeFlag, darkModeFlag);
            } else {       //清除黑色字体
                extraFlagField.invoke(window, 0, darkModeFlag);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getMeizuFlymeOSFlag() {
        return getSystemProperty("ro.build.display.id", "");
    }

    private static String getSystemProperty(String key, String defaultValue) {
        try {
            Class<?> clz = Class.forName("android.os.SystemProperties");
            Method get = clz.getMethod("get", String.class, String.class);
            return (String) get.invoke(clz, key, defaultValue);
        } catch (Exception e) {
        }
        return defaultValue;
    }

}
