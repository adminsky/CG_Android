package com.lmm.cg.zjgcg.service;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;

public interface UploadAction {

    @POST("GetCity")
    Observable<ReturnInfo<HashMap<String, String>>> getCityAction();


    //licenseplate
    @Multipart
    @POST("licenseplate")
    Observable<HashMap<String, Object>> uploadOCRCar(@PartMap Map<String, RequestBody> requestBodyMap);


    @Multipart
    @POST("ocridcard")
    Observable<HashMap<String, Object>> uploadOCRPerson(@PartMap Map<String, RequestBody> requestBodyMap);


    //检测更新
    @Headers({"Accept:application/json", "Content-Type:application/x-www-form-urlencoded"})
    @POST("getVersion")
    Observable<ReturnInfo<HashMap<String, String>>> getVersion(@QueryMap Map<String, String> requestBodyMap);

    // 简易程序
    @Headers("Content-type:application/x-www-form-urlencoded;charset=UTF-8")
    @FormUrlEncoded
    @POST("UploadFile_JYCX")
    Observable<HashMap<String, Object>> uploadJYCX(@FieldMap Map<String,String> requestBodyMap);


}
