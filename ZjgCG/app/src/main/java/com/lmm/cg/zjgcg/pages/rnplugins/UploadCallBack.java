package com.lmm.cg.zjgcg.pages.rnplugins;

/**
 * @author xmgong
 */
public interface UploadCallBack {
    void uploadFinish(String netaddress);
    void uploadFail();
}