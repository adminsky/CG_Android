package com.lmm.cg.zjgcg.pages.choosephoto;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lmm.cg.zjgcg.R;

import java.util.List;

public class PhotoSimpleAdapter extends BaseQuickAdapter<PhotoModal, BaseViewHolder> {
    PhotoDelInterface delInterface;
    Context context;

    public PhotoSimpleAdapter(@Nullable List<PhotoModal> data) {
        super(R.layout.adapter_photo_simple, data);
    }

    public void setContext(Context con) {
        this.context = con;
    }

    public void setDelInterface(PhotoDelInterface de) {
        this.delInterface = de;
    }

    @Override
    protected void convert(final BaseViewHolder helper, PhotoModal item) {

        if (!item.getNetAddress().equals("")) {
            Glide.with(context).load(item.getNetAddress()).override(300, 300).into((ImageView) helper.getView(R.id.iv_photo));
        } else {
            helper.setImageBitmap(R.id.iv_photo, BitmapFactory.decodeFile(item.getFilePath()));
        }

        helper.getView(R.id.btn_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delInterface.delAction(helper.getAdapterPosition());
            }
        });
    }
}
