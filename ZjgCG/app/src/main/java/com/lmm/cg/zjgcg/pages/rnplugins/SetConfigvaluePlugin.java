package com.lmm.cg.zjgcg.pages.rnplugins;

import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.config.AppKeyValueUtils;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;

import java.util.Map;

/**
 * @author xmgong
 * @date 2017/12/2
 */
@RNPlugin(method = "setconfigvalue")
public class SetConfigvaluePlugin extends BasePlugin<RNMainActivity> {
    public SetConfigvaluePlugin(RNMainActivity act) {
        super(act);
    }

    @Override
    protected void doAction(Map<String, Object> map, Callback callback) {
        if (map.containsKey("key") && map.containsKey("value")) {
            String key = map.get("key").toString();
            String value = map.get("value").toString();
            if (key.length() > 0 && value != null) {
                AppKeyValueUtils.setValue(key, value);
            }
        }
    }
}
