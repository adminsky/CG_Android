package com.lmm.cg.zjgcg.actions;

import com.lmm.cg.zjgcg.service.ReturnInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface PrintDetailAction {

    @Headers({"Accept:application/json", "Content-Type:application/x-www-form-urlencoded"})
    @POST("GetWeiZhangTCInfo")
    Observable<ReturnInfo<ArrayList<HashMap<String,String>>>> getWeiZhangTCInfo(@Body RequestBody res);


}
