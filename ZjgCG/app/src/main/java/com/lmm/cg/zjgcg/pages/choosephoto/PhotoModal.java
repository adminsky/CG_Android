package com.lmm.cg.zjgcg.pages.choosephoto;

import java.io.File;

public class PhotoModal {

    private String bigFilePath = "";
    private String filePath = "";
    private File mfile = null;
    private boolean isUpload = false;
    private String netAddress = "";
    private String ajbh = ""; //案件编号

    public String getAjbh() {
        return ajbh;
    }

    public void setAjbh(String ajbh) {
        this.ajbh = ajbh;
    }

    public String getBigFilePath() {
        return bigFilePath;
    }

    public void setBigFilePath(String bigFilePath) {
        this.bigFilePath = bigFilePath;
    }

    public String getNetAddress() {
        return netAddress;
    }

    public void setNetAddress(String netAddress) {
        this.netAddress = netAddress;
    }

    public boolean isUpload() {
        return isUpload;
    }

    public void setUpload(boolean upload) {
        isUpload = upload;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public File getMfile() {
        return mfile;
    }

    public void setMfile(File mfile) {
        this.mfile = mfile;
    }
}
