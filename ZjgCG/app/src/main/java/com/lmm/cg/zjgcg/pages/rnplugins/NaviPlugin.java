package com.lmm.cg.zjgcg.pages.rnplugins;

import android.content.Intent;
import android.net.Uri;

import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;
import com.lmm.dresswisdom.lmmframe.util.PackageUtils;
import com.lmm.dresswisdom.lmmframe.util.ToastUtils;

import java.util.Map;

@RNPlugin(method = "bd_navi")
public class NaviPlugin extends BasePlugin<RNMainActivity> {

    public NaviPlugin(RNMainActivity act) {
        super(act);
    }

    @Override
    protected void doAction(Map<String, Object> map, Callback callback) {
        String lat = map.get("lat").toString();
        String lng = map.get("lon").toString();

        if (PackageUtils.isAvilible(activity, "com.baidu.BaiduMap")) {
            Intent i1 = new Intent();
            i1.setData(Uri.parse("baidumap://map/navi?location=" + lat + "," + lng));
            activity.startActivity(i1);
        } else {
            ToastUtils.showToastShort("请先安装百度地图");
        }
    }
}
