package com.lmm.cg.zjgcg.actions;

import com.lmm.cg.zjgcg.service.ReturnInfo;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PhotoAction {


    // 删除照片
    @Headers({"Accept:application/json", "Content-Type:application/x-www-form-urlencoded"})
    @POST("DelPicture ")
    Observable<ReturnInfo<Object>> DeletePictureAction(@Body RequestBody res);


    // 校验图片
    @Headers({"Accept:application/json", "Content-Type:application/x-www-form-urlencoded"})
    @POST("DelRedundantPicture")
    Observable<ReturnInfo<Object>> CheckPhotoAction(@Body RequestBody res);

}
