package com.lmm.cg.zjgcg.pages.rnplugins;

import com.alibaba.android.arouter.launcher.ARouter;
import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.config.ReqCodeConfig;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;
import java.util.Map;


/**
 * 设置蓝牙链接
 */
@RNPlugin(method = "setBlueThooth")
public class SetBlueTooth extends BasePlugin<RNMainActivity> {
    public SetBlueTooth(RNMainActivity act) {
        super(act);
    }

    @Override
    protected void doAction(Map<String, Object> map, Callback callback) {
        ARouter.getInstance().build("/page/devicelist" ).navigation(activity , ReqCodeConfig.ReqCode_SETBlueTooth);
    }
}
