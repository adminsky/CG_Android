package com.lmm.cg.zjgcg.pages.choosephoto;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.lmm.cg.zjgcg.R;
import com.lmm.cg.zjgcg.pages.AppBaseActivity;
import com.lmm.cg.zjgcg.pages.ReactNativePresent;
import com.lmm.cg.zjgcg.pages.rnplugins.UploadCallBack;
import com.lmm.dresswisdom.lmmframe.util.ToastUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


@Route(path = "/page/photolist")
public class PhotoListActivity extends AppBaseActivity implements View.OnClickListener {

    RecyclerView listView;
    Button btnTakePhoto;//拍照按钮
    Button btnComplete;//拍照完成
    TextView tvTitle;
    ImageView ivBack;
    LinearLayout relTopbar;
    PhotoAdapter mAdaper;
    List<PhotoModal> photoModalList = new ArrayList<>();

    ReactNativePresent mPresent;
    @Autowired(name = "type")
    public String uploadType = "";
    @Autowired(name = "guid")
    public String recordGuid = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ARouter.getInstance().inject(this);
        setContentView(R.layout.activity_photolist);
        hideActionBar();


        initView();
        mAdaper = new PhotoAdapter(photoModalList);

        mAdaper.setDelInterface(new PhotoDelInterface() {
            @Override
            public void delAction(int index) {
                photoModalList.remove(index);
                mAdaper.notifyDataSetChanged();
            }
        });
        listView.setAdapter(mAdaper);
        mPresent = new ReactNativePresent();
    }


    private void initView() {
        listView = (RecyclerView) findViewById(R.id.recy_photo);
        listView.setLayoutManager(new LinearLayoutManager(this));
        btnTakePhoto = (Button) findViewById(R.id.btn_take);
        btnComplete = (Button) findViewById(R.id.btn_complete);
        btnComplete.setOnClickListener(this);
        btnTakePhoto.setOnClickListener(this);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText("照片列表");
        ivBack = (ImageView) findViewById(R.id.ivBack);
        relTopbar = (LinearLayout) findViewById(R.id.relTopbar);
        relTopbar.getLayoutParams().height = relTopbar.getLayoutParams().height + getStatusBarHeight();
        relTopbar.setPadding(relTopbar.getPaddingLeft(), getStatusBarHeight(), relTopbar.getPaddingRight(), relTopbar.getPaddingBottom());
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnTakePhoto) {
            ARouter.getInstance().build("/page/takephoto").navigation(this, 123);
        } else if (v == ivBack) {
            finish();
        } else if (v == btnComplete) {
            showDefaultDialog();
            uploadAllFile();
        }
    }


    private void uploadAllFile() {
        for (PhotoModal item : photoModalList) {
            if (!item.isUpload()) {
                uploadItem(item);
                return;
            }
        }

        hideDefaultDialog();

        // 全部上传完成
        // 回掉
        Intent mintent = new Intent();
        String imgUrls = "";
        for (PhotoModal item : photoModalList) {
            imgUrls += item.getNetAddress() + ";";
        }
        mintent.putExtra("imgs", imgUrls);
        setResult(RESULT_OK, mintent);
        finish();
    }

    private void uploadItem(final PhotoModal image) {
        if (uploadType.equals("jycxPj") || uploadType.equals("jycxZj")) {
//            mPresent.uploadFileJYCX(recordGuid, new File(image.getFilePath()), uploadType, new UploadCallBack() {
//                @Override
//                public void uploadFinish(String netaddress) {
//                    image.setUpload(true);
//                    image.setNetAddress(netaddress);
//                    uploadAllFile();
//                }
//
//                @Override
//                public void uploadFail() {
//                    hideDefaultDialog();
//                    ToastUtils.showToastShort("图片上传出错");
//                    image.setUpload(false);
//                }
//            });
        } else if (uploadType.equals("ybaj") || uploadType.equals("wxcl")) {
            mPresent.uploadFileMessage(recordGuid, new File(image.getFilePath()), uploadType, new UploadCallBack() {
                @Override
                public void uploadFinish(String netaddress) {
                    image.setUpload(true);
                    image.setNetAddress(netaddress);
                    uploadAllFile();
                }

                @Override
                public void uploadFail() {
                    hideDefaultDialog();
                    ToastUtils.showToastShort("图片上传出错");
                    image.setUpload(false);
                }
            });
        } else {
            mPresent.uploadFile(recordGuid, new File(image.getFilePath()), new UploadCallBack() {
                @Override
                public void uploadFinish(String netaddress) {
                    image.setUpload(true);
                    image.setNetAddress(netaddress);
                    uploadAllFile();
                }

                @Override
                public void uploadFail() {
                    hideDefaultDialog();
                    ToastUtils.showToastShort("图片上传出错");
                    image.setUpload(false);
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123) {
            if (resultCode == RESULT_OK) {
                String[] imgs = data.getStringArrayExtra("imgs");
                PhotoModal photoModal;
                for (String item : imgs) {
                    photoModal = new PhotoModal();
                    photoModal.setFilePath(item);
                    photoModalList.add(photoModal);
                }
                mAdaper.notifyDataSetChanged();
            }
        }
    }
}
