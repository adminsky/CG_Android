package com.lmm.cg.zjgcg.service;

public class UpdateAppMessage {
    public int state = 0; //
    public int max = 0;
    public int progress = 0;
    public String filePath = "";

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public UpdateAppMessage(int state, int max, int progress) {
        this.state = state;
        this.max = max;
        this.progress = progress;
    }
}
