package com.lmm.cg.zjgcg.task;

public abstract class TaskCallBack<S> {

    public abstract void onSuccess(S s);
    public abstract void onFail(String s);
    public abstract void onFinish();

}
