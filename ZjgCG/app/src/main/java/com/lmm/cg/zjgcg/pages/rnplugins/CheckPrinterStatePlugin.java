package com.lmm.cg.zjgcg.pages.rnplugins;


import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.lmm.cg.zjgcg.config.AppKey;
import com.lmm.cg.zjgcg.config.AppKeyValueUtils;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;
import com.lmm.dresswisdom.lmmframe.util.LogUtils;
import com.qr.print.PrintPP_CPCL;

import java.util.Map;

@RNPlugin(method = "checkPrinterState")
public class CheckPrinterStatePlugin extends BasePlugin<RNMainActivity> {
    RNMainActivity activity;

    public CheckPrinterStatePlugin(RNMainActivity act) {
        super(act);
        activity = act;
    }

    @Override
    protected void doAction(Map<String, Object> map, Callback callback) {



        String name = AppKeyValueUtils.getValue(AppKey.BlueToothName);
        String address = AppKeyValueUtils.getValue(AppKey.BlueToothAddress);
        boolean isConnect = false;

        LogUtils.write(isConnect ? "OK": "Fail");
        WritableMap writableMap = Arguments.createMap();
        writableMap.putString("state", isConnect? "1":"0");
        writableMap.putString("address", address);
        writableMap.putString("name", name);
        callback.invoke(writableMap);
    }
}
