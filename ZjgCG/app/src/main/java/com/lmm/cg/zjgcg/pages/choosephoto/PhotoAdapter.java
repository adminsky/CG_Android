package com.lmm.cg.zjgcg.pages.choosephoto;

import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.CheckBox;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lmm.cg.zjgcg.R;
import com.lmm.dresswisdom.lmmframe.util.LogUtils;

import java.util.List;

public class PhotoAdapter extends BaseQuickAdapter<PhotoModal, BaseViewHolder> {

    PhotoDelInterface delInterface;

    public PhotoAdapter(@Nullable List<PhotoModal> data) {
        super(R.layout.adapter_photo, data);
    }

    public void setDelInterface(PhotoDelInterface de) {
        this.delInterface = de;
    }

    @Override
    protected void convert(final BaseViewHolder helper, final PhotoModal item) {
        helper.setImageBitmap(R.id.iv_photo, BitmapFactory.decodeFile(item.getFilePath()));

        helper.getView(R.id.btn_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delInterface.delAction(helper.getAdapterPosition());
            }
        });
    }
}
