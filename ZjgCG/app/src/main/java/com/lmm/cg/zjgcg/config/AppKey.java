package com.lmm.cg.zjgcg.config;

/**
 *
 * @author xmgong
 * @date 2017/11/15
 * 配置项
 * 原生使用的配置项
 */

public class AppKey {
    public static final String LoginState = "loginstate";
    public static final String UserId = "userid";
    public static final String UserGuid = "UserGuid";
    public static final String Password = "password";
    public static final String IM_Account = "im_account";


    //当前位置不同于坐标位置
    public static final String CurrentPlace = "currentplace";
    public static final String CurrentPlaceCode = "currentplacecode";
    public static final String HistoryCitys = "historycity";
    public static final String HistoryKeys = "historykeys";

    public static final String UserPwd = "userpwd";
    public static final String Token = "token";
    public static final String ShopKey = "spk";
    public static final String Mobile = "mobile";
    public static final String UserName = "username";
    public static final String UserSex = "usersex";
    public static final String UserBirth = "userbirth";
    public static final String UserHead = "userhead";

    public static final String ClientId = "clientid";

    public static final String BlueToothAddress = "bluetoothaddress";
    public static final String BlueToothName = "bluetoothname";
    public static final String BlueToothState = "bluetoothstate";
    public static final String BLUETOOTH_TAG = "BLUETOOTH_TAG";
}

