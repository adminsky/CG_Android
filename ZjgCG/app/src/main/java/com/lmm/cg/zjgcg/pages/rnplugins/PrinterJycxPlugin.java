package com.lmm.cg.zjgcg.pages.rnplugins;

import com.facebook.react.bridge.Callback;
import com.lmm.cg.zjgcg.R;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;

import java.util.Map;

@RNPlugin(method = "printerJycx")
public class PrinterJycxPlugin extends BasePlugin<RNMainActivity> {


    RNMainActivity activity;

    public PrinterJycxPlugin(RNMainActivity act) {
        super(act);
        activity = act;
    }

    @Override
    protected void doAction(final Map<String, Object> map, final Callback callback) {
        activity.addCallBack("printerJycx", callback);
        JQPrintAction1(map);

    }


    /**
     * 济强旧版打印
     */
    private void JQPrintAction1(Map<String, Object> map) {
        String cfNo = "";
        String dsrPerson = "";
        String dsrId = "";
        String printDate = ""; // 年月日
        String cfAddress = "";
        String cfXw = ""; // 处罚行为
        String wfYj = ""; // 违法依据
        String cfYj = ""; // 处罚依据
        String cfJe = ""; //   处罚金额
        String zfrName = ""; // 执法人员name
        String zfrId = map.get("zfrId").toString(); // 执法人员 loginId
        String reportCompany = ""; // 报告单位

        try {
            cfNo = new String(map.get("cfNo").toString().getBytes(), "UTF-8");
            dsrPerson = new String(map.get("dsrPerson").toString().getBytes(), "UTF-8");
            dsrId = new String(map.get("dsrId").toString().getBytes(), "UTF-8");
            printDate = new String(map.get("printDate").toString().getBytes(), "UTF-8");
            cfAddress = new String(map.get("cfAddress").toString().getBytes(), "UTF-8");
            cfXw = new String(map.get("cfXw").toString().getBytes(), "UTF-8");
            wfYj = new String(map.get("wfYj").toString().getBytes(), "UTF-8");
            cfYj = new String(map.get("cfYj").toString().getBytes(), "UTF-8");
            cfJe = new String(map.get("cfJe").toString().getBytes(), "UTF-8");
            zfrName = new String(map.get("zfrName").toString().getBytes(), "UTF-8");
            zfrId = new String(map.get("zfrId").toString().getBytes(), "UTF-8");
            reportCompany = new String(map.get("reportCompany").toString().getBytes(), "UTF-8"); // 报告单位
        } catch (Exception e) {

        }

        String message1 = activity.getResources().getString(R.string.JYCXContent);
        message1 = message1.replace("[标题1]", "              " + reportCompany);
        message1 = message1.replace("[标题2]", "           行  政  处  罚  决  定  书");
        message1 = message1.replace("[处罚编号]", "                              NO:" + cfNo);
        message1 = message1.replace("[正文1]", "公民：" + dsrPerson + "  身份证号码：" + dsrId);
        message1 = message1.replace("[正文2]", "   现查明，当事人于" + printDate + "在" + cfAddress + "进行" + cfXw + "的行为。该行为违反了" + wfYj + "的规定，现责令你立即改正，并依据" + cfYj + "的规定，决定给予你" + cfJe + "元罚款的行政处罚。");
        message1 = message1.replace("[正文3]", "   逾期不缴纳罚款的，每日按罚款数额的3%加处罚额。");
        message1 = message1.replace("[正文4]", "   如不服本决定者，" +
                "可在接到本决定书之日起60日内向张家港市人民政府申请复议，也可在接到本决定书之日起6个月内向人民法院提起诉讼。逾期不申请复议，也不向人民法院起诉，又不履行本决定的，本机关将申请人民法院强制执行。复议或诉讼期间，本决定不停止执行。");
        message1 = message1.replace("[正文5]", "执法人员：" + zfrName + " " + zfrId + "  处罚地点：" + cfAddress);
        message1 = message1.replace("[监制]", "            " + reportCompany + "(印章)");
        message1 = message1.replace("[年月日]", "                              " + printDate);
        message1 = message1.replace("[当事人]", "当事人：");
        message1 = message1.replace("[当事人日期]", "日期：      年      月      日");
        message1 = message1.replace("[空白]", "          （第一联   执法部门存档）");

        String message = activity.getResources().getString(R.string.JYCXContent);
        message = message.replace("[标题1]", "              " + reportCompany);
        message = message.replace("[标题2]", "           行  政  处  罚  决  定  书");
        message = message.replace("[处罚编号]", "                              NO:" + cfNo);
        message = message.replace("[正文1]", "公民：" + dsrPerson + "  身份证号码：" + dsrId);
        message = message.replace("[正文2]", "   现查明，当事人于" + printDate + "在" + cfAddress + "进行" + cfXw + "的行为。该行为违反了" + wfYj + "的规定，现责令你立即改正，并依据" + cfYj + "的规定，决定给予你" + cfJe + "元罚款的行政处罚。");
        message = message.replace("[正文3]", "   逾期不缴纳罚款的，每日按罚款数额的3%加处罚额。");
        message = message.replace("[正文4]", "   如不服本决定者，" +
                "可在接到本决定书之日起60日内向张家港市人民政府申请复议，也可在接到本决定书之日起6个月内向人民法院提起诉讼。逾期不申请复议，也不向人民法院起诉，又不履行本决定的，本机关将申请人民法院强制执行。复议或诉讼期间，本决定不停止执行。");
        message = message.replace("[正文5]", "执法人员：" + zfrName + " " + zfrId + "  处罚地点：" + cfAddress);
        message = message.replace("[监制]", "            " + reportCompany + "(印章)");
        message = message.replace("[年月日]", "                              " + printDate);
        message = message.replace("[当事人]", "当事人：");
        message = message.replace("[当事人日期]", "日期：      年      月      日");
        message = message.replace("[空白]", "          （第二联   当事人执收）");


        activity.connectAndPrint(message1, message);

    }

}
