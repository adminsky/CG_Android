package com.lmm.cg.zjgcg.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

/**
 * Created by xmgong on 2018/1/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnInfo<T> implements Serializable {
    private int result = 0;
    private String msg = "";
    private T data = null;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


    public boolean isSuccess(){
        return result == 200;
    }
}
