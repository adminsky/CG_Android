package com.lmm.cg.zjgcg.pages.rnplugins;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.lmm.cg.zjgcg.MainApplication;
import com.lmm.cg.zjgcg.config.AppKey;
import com.lmm.cg.zjgcg.config.AppKeyValueUtils;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.cg.zjgcg.pages.bluetooth.BluetoothCallBack;
import com.lmm.cg.zjgcg.pages.bluetooth.BluetoothService;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;
import com.lmm.dresswisdom.lmmframe.util.ToastUtils;
import com.qr.print.PrintPP_CPCL;

import java.util.Map;

@RNPlugin(method = "contectPrinter")
public class ContectPrinterPlugin extends BasePlugin<RNMainActivity> {
    RNMainActivity activity;
    PrintPP_CPCL mPrinter;
    BluetoothAdapter mBluetoothAdapter;

    String state = "0";

    public ContectPrinterPlugin(RNMainActivity act) {
        super(act);
        activity = act;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            ToastUtils.showToastShort("蓝牙设备不可用");
        }
    }

    @Override
    protected void doAction(Map<String, Object> map, final Callback callback) {
        String name = AppKeyValueUtils.getValue(AppKey.BlueToothName);
        String address = AppKeyValueUtils.getValue(AppKey.BlueToothAddress);

        if (name.length() > 0) {
            WritableMap writableMap = Arguments.createMap();
//            if (name.startsWith("QR")) {
//                if (activity.getPrintPPCpcl().connect(name, address)) {
//                    state = "1";
//                    writableMap.putString("address", address);
//                    writableMap.putString("name", name);
//                } else {
//                    ToastUtils.showToastShort("重启设备试试");
//                    state = "0";
//                }
//                writableMap.putString("state", state);
//                callback.invoke(writableMap);
//            } else {

//                if (activity.getPrinter() != null) {
//                    activity.getPrinter().close();
//                }
//
//                if (!activity.getPrinter().open(address)) {
//                    Toast.makeText(activity, "打印机Open失败", Toast.LENGTH_SHORT).show();
//                    state = "0";
//                } else {
//                    state = "1";
//                }
//
//                if (!activity.getPrinter().wakeUp()) {
//                    state = "0";
//                }
//
//
//                writableMap.putString("address", address);
//                writableMap.putString("name", name);
//                ((MainApplication) activity.getApplication()).printer = activity.getPrinter();
//                writableMap.putString("state", state);
//                callback.invoke(writableMap);
//            }
        }

    }

    /**
     * 连接打印机 旧方法
     */
//    private void connectBluetooth2(String name , String address) {
//        if (mService == null) {
//            mService = new BluetoothService(RNMainActivity.this , mHandler);
//        }
//
//        if (address.length() > 0) {
//            BluetoothDevice device = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
//            mService.connect(device);
//        }
//    }

    /**
     * 连接打印机 新方法
     */
//    private void connectBluetooth1(String name , String address){
//        if (printer != null) {
//            printer.close();
//        }
//
//        if (!printer.open(address)) {
//            Toast.makeText(this, "打印机Open失败", Toast.LENGTH_SHORT).show();
//            return;
//        }
//
//
//        if (!printer.wakeUp())
//            return;
//
//        mApplication.printer = printer;
//    }

}
