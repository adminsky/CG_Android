package com.lmm.cg.zjgcg.config;

/**
 * Created by xmgong on 2018/2/2.
 */

public class ReqCodeConfig {
    public static int ReqCode_SETBlueTooth = 91;
    public static int ReqCode_ChoosePoint = 90;
    public static int ReqCode_ENABLE_BT = 92;

    public static int ReqCode_ChoosePhoto = 93;
    public static int ReqCode_ChoosePhotoFromAlumb = 94;
    public static int ReqCode_TakePhoto = 95;

    public static int ReqCode_TakePhotoOCR = 96;
}
