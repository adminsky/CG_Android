package com.lmm.cg.zjgcg.pages.rnplugins;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.lmm.cg.zjgcg.config.AppKeyValueUtils;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;

import java.util.Map;

/**
 * @author xmgong
 * @date 2017/11/28
 */
@RNPlugin(method = "getconfigvalue")
public class GetConfigvaluePlugin extends BasePlugin<RNMainActivity> {
    public GetConfigvaluePlugin(RNMainActivity act) {
        super(act);
    }

    @Override
    protected void doAction(Map<String, Object> paramsMap, Callback callback) {
        String keys = paramsMap.get("keys").toString();
        WritableMap writableMap = Arguments.createMap();
        String[] keyitems = keys.split(",");
        String value = "";
        for (String item : keyitems) {
            value = AppKeyValueUtils.getValue(item);
            writableMap.putString(item, value);
        }
        callback.invoke(writableMap);
    }
}
