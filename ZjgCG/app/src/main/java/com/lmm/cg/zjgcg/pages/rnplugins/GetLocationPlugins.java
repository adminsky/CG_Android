package com.lmm.cg.zjgcg.pages.rnplugins;

import com.baidu.location.BDLocation;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;
import com.lmm.cg.zjgcg.pages.RNMainActivity;
import com.lmm.cg.zjgcg.utils.BDGPSUtils;
import com.lmm.dresswisdom.lmmframe.reactnative.BasePlugin;
import com.lmm.dresswisdom.lmmframe.reactnative.annotation.RNPlugin;


import java.util.Map;

/**
 * Created by xmgong on 2018/3/17.
 */
@RNPlugin(method = "getlocation")
public class GetLocationPlugins extends BasePlugin<RNMainActivity> {

    BDGPSUtils bdgpsUtils = null;

    public GetLocationPlugins(RNMainActivity act) {
        super(act);
        bdgpsUtils = new BDGPSUtils(new BDGPSUtils.GPSReturn() {

            @Override
            public void retrunGPSInfo(BDLocation bdLocation) {
                if (bdLocation != null) {
                    String address = bdLocation.getStreet();
                    double lat = bdLocation.getLatitude();
                    double log = bdLocation.getLongitude();
                    WritableMap writableMap = Arguments.createMap();
                    writableMap.putString("address", address);
                    writableMap.putDouble("lat", lat);
                    writableMap.putDouble("lon", log);

                    getCallBack().invoke(writableMap);

                    bdgpsUtils.stopGPS();
                }
            }
        }, activity);

    }

    @Override
    protected void doAction(Map<String, Object> map, Callback callback) {
        bdgpsUtils.startGPS();
    }
}
