package com.lmm.cg.zjgcg.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.ksoap2.transport.ServiceConnection;

public class ServiceConnectionSE implements ServiceConnection {
	private HttpURLConnection connection;

	public ServiceConnectionSE(String url) throws IOException {
		this.connection = ((HttpURLConnection) new URL(url).openConnection());
		this.connection.setUseCaches(false);
		this.connection.setDoOutput(true);
		this.connection.setDoInput(true);
	}

	@Override
	public void connect() throws IOException {
		this.connection.connect();
	}

	@Override
	public void disconnect() {
		this.connection.disconnect();
	}

	@Override
	public void setRequestProperty(String string, String soapAction) {
		this.connection.setRequestProperty(string, soapAction);
	}

	@Override
	public void setRequestMethod(String requestMethod) throws IOException {
		this.connection.setRequestMethod(requestMethod);
	}

	@Override
	public OutputStream openOutputStream() throws IOException {
		return this.connection.getOutputStream();
	}

	@Override
	public InputStream openInputStream() throws IOException {
		return this.connection.getInputStream();
	}

	@Override
	public InputStream getErrorStream() {
		return this.connection.getErrorStream();
	}

	// �������ӷ������ĳ�ʱʱ��,���뼶,��Ϊ�Լ���ӵķ���
	public void setConnectionTimeOut(int timeout) {
		this.connection.setConnectTimeout(timeout);
	}

	@Override
	public String getHost() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPath() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPort() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List getResponseProperties() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}