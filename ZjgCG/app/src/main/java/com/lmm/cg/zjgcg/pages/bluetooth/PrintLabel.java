package com.lmm.cg.zjgcg.pages.bluetooth;

import android.graphics.Bitmap;
import android.util.Log;

import com.qr.print.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.SimpleFormatter;

public class PrintLabel {
//    public static void Lable(PrintPP_CPCL iPrinter) {
//        iPrinter.pageSetup(586, 1436);
//        //第一联
//        iPrinter.drawLine(2, 0, 240, 568 + 16, 240, false);//第一联横线1
//
//        iPrinter.drawLine(2, 0, 384, 568 + 16, 384, false);//第一联横线2
//
//        iPrinter.drawLine(2, 0, 552, 568 - 32 + 8, 552, false);//第一联横线3
//
//        iPrinter.drawLine(2, 40, 384, 40, 680, false);//第一联竖线1，从左到右
//
//        iPrinter.drawLine(2, 408, 552, 408, 680, false);//第一联竖线2，从左到右
//
//        iPrinter.drawLine(2, 568 - 32 + 8, 384, 568 - 32 + 8, 680, false);//第一联竖线3，从左到右
//        //三段码
//        iPrinter.drawText(2 + 20, 128 + 16 + 8, "210-123-000", 6, 0, 0, false, false);
//        //收件人
//        iPrinter.drawText(2 + 4, 384 + 28, 32, 120, "收件人", 3, 0, 1, false, false);
//        //收件人姓名＋电话，最终实施时请用变量替换
//        iPrinter.drawText(2 + 4 + 32 + 8, 264 + 128, 480, 32, "张三" + " " + "18212345678", 3, 0, 1, false, false);
//        //收件地址 ，最终实施时请用变量替换
//        iPrinter.drawText(2 + 4 + 32 + 8, 372 + 40 + 22, 448, 120, "湖南省" + "湘潭市" + "雨湖区" + " " + "雨湖区政府1234号", 3, 0, 1, false, false);
//        //寄件人
//        iPrinter.drawText(2 + 8, 552 + 22, 32, 96, "寄件人", 2, 0, 0, false, false);
//        //寄件人姓名＋电话，
//        iPrinter.drawText(2 + 4 + 32 + 8, 552 + 8, 480, 24, "李四" + " " + "15188889999", 2, 0, 0, false, false);
//        //寄件人地址
//        iPrinter.drawText(2 + 4 + 32 + 8, 552 + 40, 344, 112, "上海" + "上海市" + "青浦区" + " " + "华徐公路3029弄28号", 2, 0, 0, false, false);
////		//签收人
//        iPrinter.drawText(2 + 424 - 8, 552 + 8, "签收人：", 2, 0, 0, false, false);
////		//日期
//        iPrinter.drawText(2 + 424, 680 - 26 - 8, "日期：", 2, 0, 0, false, false);
////		//派件联
//        iPrinter.drawText(568 - 32 + 3 + 8, 384 + 128, 32, 96, "派件联", 2, 0, 0, false, false);
//
//        iPrinter.drawLine(2, 0, 696 + 32, 568 + 16, 696 + 32, false);//第二联横线1，从左到右
//
//        iPrinter.drawLine(2, 0, 696 + 160, 568 - 32 + 16, 696 + 160, false);//第二联横线2，从左到右
//
//        iPrinter.drawLine(2, 40, 696 + 160 + 96, 568 - 32 + 8, 696 + 160 + 96, false);//第二联横线3，从左到右
//
//        iPrinter.drawLine(2, 40, 696 + 32, 40, 696 + 288, false);//第二联竖线1，从左到右
//
//        iPrinter.drawLine(2, 248 + 42, 696 + 160 + 96, 248 + 42, 680 + 16 + 288, false);//第二联竖线2，从左到右
//
//        iPrinter.drawLine(2, 568 - 32 + 8 - 96, 696 + 160, 568 - 32 + 8 - 96, 696 + 160 + 96, false);//第二联竖线3，从左到右
//
//        iPrinter.drawLine(2, 568 - 32 + 8, 696 + 32, 568 - 32 + 8, 680 + 16 + 288, false);//第二联竖线4，从左到右
//
//        //运单号+运单号
//        iPrinter.drawText(8, 696 + 3, "运单号：" + "1234567890" + " □" + "订单号：" + "LP123456789", 2, 0, 0, false, false);
//        //收件人
//        iPrinter.drawText(2 + 4, 696 + 32 + 16, 32, 96, "收件人", 2, 0, 0, false, false);
//        //收件人姓名＋电话，最终实施时请用变量替换
//        iPrinter.drawText(2 + 8 + 32 + 8, 608 + 128, 480, 24, "张三" + " " + "18212345678", 2, 0, 0, false, false);
//        //收件地址 ，最终实施时请用变量替换
//        iPrinter.drawText(2 + 8 + 32 + 8, 696 + 32 + 40 + 2, 424, 80, "湖南省" + "湘潭市" + "雨湖区" + " " + "雨湖区政府1234号", 2, 0, 0, false, false);
////		//内容品名
//        iPrinter.drawText(2 + 4, 696 + 160 + 3, 32, 120, "内容品名", 2, 0, 0, false, false);
//        //内容品名具体
//        iPrinter.drawText(2 + 4 + 32 + 8 + 4 + 4, 696 + 160 + 8, 432 - 100, 136, "鞋子、衣服、鞋子、衣服、鞋子、衣服、鞋子、衣服、鞋子、衣服、鞋子、衣服、", 2, 0, 0, false, false);
//        //面单模式 A ：A网
//        iPrinter.drawText(568 - 32 + 8 - 96 + 24, 696 + 160 + 2, "A", 7, 0, 0, false, false);
//        //数量
//        iPrinter.drawText(2 + 4 + 32 + 8, 696 + 160 + 96 + 4, "数量：" + "1", 2, 0, 0, false, false);
//        //重量
//        iPrinter.drawText(248 + 42 + 4, 696 + 160 + 96 + 4, "重量：" + "1" + "kg", 2, 0, 0, false, false);
////		//收件联
//        iPrinter.drawText(568 - 32 + 3 + 8, 696 + 32 + 80, 32, 96, "收件联", 2, 0, 0, false, false);
//        //虚线
//
//        iPrinter.drawLine(2, 0, 1096, 568 + 16, 1096, false);//第三联横线1，从左到右
//
//        iPrinter.drawLine(2, 0, 1096 + 104 - 8 + 4, 568 - 32 + 8, 1096 + 104 - 8 + 4, false);//第三联横线2，从左到右
//
//        iPrinter.drawLine(2, 0, 1096 + 104 + 104 - 8, 568 - 32 + 8, 1096 + 104 + 104 - 8, false);//第三联横线3，从左到右
//
//        iPrinter.drawLine(2, 40, 1096 + 104 + 104 + 96 + 4 - 4 - 2 - 8 - 4, 568 - 32 + 8, 1096 + 104 + 104 + 96 + 4 - 4 - 2 - 8 - 4, false);//第三联横线4，从左到右
//
//
//        iPrinter.drawLine(2, 40, 1096, 40, 1432 - 4 - 16 + 4, false);//第三联竖线1，从左到右
//
//        iPrinter.drawLine(2, 248 + 42, 1096 + 104 + 104 + 96 - 8 + 4, 248 + 42, 1432 - 4 - 16 + 4, false);//第三联竖线2，从左到右
//
//        iPrinter.drawLine(2, 568 - 32 + 8, 1096, 568 - 32 + 8, 1432 - 4 - 16 + 4, false);//第三联竖线3，从左到右
//
//        //收件人
//        iPrinter.drawText(2 + 4, 1096 + 5, 32, 96, "收件人", 2, 0, 0, false, false);
//        //收件人姓名＋电话，最终实施时请用变量替换
//        iPrinter.drawText(2 + 8 + 32 + 8 + 4 + 4, 1096 + 8, 480, 24, "张三" + " " + "18212345678", 2, 0, 0, false, false);
//        //收件地址 ，最终实施时请用变量替换
//        iPrinter.drawText(2 + 8 + 32 + 8 + 4 + 4, 1096 + 8 + 24 + 8, 456, 64, "湖南省" + "湘潭市" + "雨湖区" + " " + "雨湖区政府1234号", 2, 0, 0, false, false);
//        //寄件人
//        iPrinter.drawText(2 + 4, 1096 + 104 + 5, 32, 96, "寄件人", 2, 0, 0, false, false);
//        //寄件人姓名＋电话，
//        iPrinter.drawText(2 + 4 + 32 + 8, 1096 + 104 + 8, 480, 24, "李四" + " " + "15188889999", 2, 0, 0, false, false);
//        //寄件人地址
//        iPrinter.drawText(2 + 4 + 32 + 8, 1096 + 104 + 8 + 24 + 8, 456, 72, "上海" + "上海市" + "青浦区" + " " + "华徐公路3029弄28号", 2, 0, 0, false, false);
//        //内容品名
//        iPrinter.drawText(2 + 4, 1096 + 104 + 104 + 1, 32, 120, "内容品名", 2, 0, 0, false, false);
//        //内容品名具体
//        iPrinter.drawText(2 + 4 + 32 + 8 + 4 + 4, 1096 + 104 + 104 + 8, 432, 156, "衣服", 2, 0, 0, false, false);
//        //数量
//        iPrinter.drawText(2 + 4 + 32 + 8, 1432 - 32 + 4 - 4 - 8 - 4, "数量：" + "1", 2, 0, 0, false, false);
//        //重量
//        iPrinter.drawText(248 + 42 + 4, 1432 - 32 + 4 - 4 - 8 - 4, "重量：" + "1" + "kg", 2, 0, 0, false, false);
//        //寄件联
//        iPrinter.drawText(568 - 32 + 3 + 8, 1096 + 104 + 16, 32, 96, "寄件联", 2, 0, 0, false, false);
//
//        iPrinter.print(0, 0);
//
//    }

    /**
     * 每行46个字节
     * 23个文字
     *
     * @param iPrinter
     */
    public static void LableNew(PrintPP_CPCL iPrinter , String cardNo , String tdbh , String tddate , String tdaddress , String CLPhone , String  CLAddress) {

        String CGAddress = CLAddress;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日");
        String dateString = formatter.format(new Date());

        iPrinter.pageSetup(580, 1460);
        //标题
        iPrinter.drawText(200, 0, "违法停车提示单", 2, 0, 1, false, false);
        // 编号
        iPrinter.drawText(340, 42 , "NO:" + tdbh, 2, 0, 1, false, false);
        // 第一段
        int linCount = LableTest(iPrinter, 20,  10 + 64, 2,     "   "+cardNo + "车主，您的车辆在下述时间、地点违法停放。");
        iPrinter.drawText(20 , 138 , "   时间：" + tddate,2 , 0 , 1, false , false);
        iPrinter.drawText(20 , 138 + 32  , "   地点：" + tdaddress,2 , 0 , 1, false , false);
        int linCount2 = LableTest(iPrinter, 20, 170 + 32, 2, "   已被拍摄并向张家港市城管行政执法局报告，请您：");
        int linCount3 = LableTest(iPrinter, 20, 170 + 32 + (linCount2 + 2) * 32, 2, "   在3日内携带有效证件到" + CGAddress +  "接受调查处理，如果您不方便到以上地址接受处理，可以在车辆审验时到张家港市公安局车辆管理所下辖的车辆检测点接受处理。");
        int linCount4 = LableTest(iPrinter,20, 170 + (linCount2 + linCount3 + 4)*32 , 2, "   处理时间：法定工作日内。联系电话:" + CLPhone);

        iPrinter.drawText(200, 180 + (linCount2 + linCount3 + linCount4  + 6 )*32, "友情提示", 2, 0, 1, false, false);
        int  linCount5 = LableTest(iPrinter , 20 , 210 +  (linCount2 + linCount3 + linCount4  + 6 )*32 , 2,"   1. 机动车所有人登记的住所地址或者联系电话发生变化时，请及时向登记车辆管理所申请变更备案。");
        int  linCount6 = LableTest(iPrinter , 20 , 210 +  (linCount2 + linCount3 + linCount4 + linCount5  + 7  )*32 , 2,"   2. 临时停放机动车的驾驶人在驾驶室的，听从指挥立即驶离的，不进行拍摄。");
        int  linCount7 = LableTest(iPrinter , 20 , 210 +  (linCount2 + linCount3 + linCount4 + linCount5  + linCount6  + 8  )*32 , 2,"   3. 在24小时内同一时间、同一地点违法停车的，可以认定为一次违法停车行为。");

        iPrinter.drawText(200, 210 +  (linCount2 + linCount3 + linCount4 + linCount5  + linCount6  + linCount7 + 12  )*32, "张家港城市管理行政执法局(监制)" , 2, 0, 1, false, false);
        iPrinter.drawText(350, 210 +  (linCount2 + linCount3 + linCount4 + linCount5  + linCount6  + linCount7 + 13  )*32,  dateString ,2, 0, 1, false, false);
        iPrinter.print(0, 0);
    }

    public static int LableTest(PrintPP_CPCL iPrinter, int x, int y, int fontSize, String info) {
        int numLength = 1000;
        String key = "";
        int keyLength = 0;
        int lineLenth = 0;
        String linStr = "";
        int linCount = 0;
        for (int i = 0; i < info.length(); i++) {
            key = info.substring(i, i + 1);
            keyLength = key.getBytes().length == 1 ? 22 : 46;
            if (lineLenth + keyLength > numLength) {
                // 超过长度
//                Log.i("Eric", "Print:" + linStr);
                iPrinter.drawText(x, y + linCount * 32, linStr, fontSize, 0, 1, false, false);

                linStr = "";
                lineLenth = 0;  // 当前字符清空
                linCount++; // 行数 + 1

                lineLenth += keyLength;
                linStr += key;
            } else {
                // 不超过长度
                lineLenth += keyLength;
                linStr += key;
            }
        }
//        Log.i("Eric", "Print:" + linStr);
        iPrinter.drawText(x, y + linCount * 32, linStr, fontSize, 0, 1, false, false);
        return linCount;
    }


    public static void LableJycx(PrintPP_CPCL iPrinter ,Map<String, Object> map ){
        String cfNo = map.get("cfNo").toString();
        String dsrPerson = map.get("dsrPerson").toString();
        String dsrId = map.get("dsrId").toString();
        String printDate = map.get("printDate").toString(); // 年月日
        String cfAddress = map.get("cfAddress").toString();
        String cfXw = map.get("cfXw").toString(); // 处罚行为
        String wfYj = map.get("wfYj").toString(); // 违法依据
        String cfYj = map.get("cfYj").toString(); // 处罚依据
        String cfJe = map.get("cfJe").toString(); //   处罚金额
        String zfrName = map.get("zfrName").toString(); // 执法人员name
        String zfrId = map.get("zfrId").toString(); // 执法人员 loginId

        String test1 = "   现查明，当事人于" + printDate + "在" + cfAddress + "进行" + cfXw + "的行为。该行为违反了" + wfYj +"的规定，现责令你立即改正，并依据"+cfYj+"的规定，决定给予你"+cfJe+"元罚款的行政处罚。";
        String test2 = "   逾期不缴纳罚款的，每日按罚款数额的3%加处罚额。";
        String test3 = "   如不服本决定者，可在接到本决定书之日起60日内向张家港市人民政府或向苏州市城市管理行政局申请复议，也可在接到本决定书之日起3个月内向人民法院提起诉讼。逾期不申请复议，也不向人民法院起诉，又不履行本决定的，本机关将申请人民法院强制执行。复议或诉讼期间，本决定不停止执行。";
        String test4 = "   执法人员:" + zfrName + " " + zfrId + "处罚地点："+ cfAddress;
        String test5 = "";
        // 标题
        iPrinter.pageSetup(580, 1100);
        iPrinter.drawText(160 , 0 , "张家港市城市管理行政局" , 2 , 0 , 1 , false , false);
        iPrinter.drawText(140 , 42 , " 行  政  处  罚  决  定  书" , 2 , 0, 1,false , false);
        iPrinter.drawText(330 , 42 + 42 , "No:" + cfNo , 2, 0,1,false,false);
        iPrinter.drawText(20 ,  42 + 42 + 42 , "公民：" + dsrPerson + "  身份证号码："+ dsrId , 2, 0 , 1, false , false);
        int linCount1  = LableTest(iPrinter, 20,  4*42 , 2,     test1);
        int linCount2  = LableTest(iPrinter , 20 , 42*4  + (linCount1 + 2) * 32  , 2 , test2 );
        int linCount3 = LableTest(iPrinter , 20 , 42*4 + (linCount1 + linCount2 + 4 )*32 , 2 , test3);
        int linCount4 = LableTest(iPrinter,20 , 42*4 + (linCount1 + linCount2 + linCount3 + 6)*32 , 2 , test4);
        iPrinter.drawText(180 ,  42*4 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 ,"张家港市城市管理行政执法局(印章)",2,0,1,false,false);
        iPrinter.drawText(20 , 42*6 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 , "当事人：", 2,0,1,false , false);
        iPrinter.drawText(20 , 42*7 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 , "日期：      年      月     日" , 2,0,1,false,false);
        iPrinter.drawText(160 , 42*9 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 , "（第一联   执法部门存档）" , 2, 0 , 1,false ,false);
        iPrinter.print(0, 0);
    }

    public static void LableJycx2(PrintPP_CPCL iPrinter ,Map<String, Object> map ){
        String cfNo = map.get("cfNo").toString();
        String dsrPerson = map.get("dsrPerson").toString();
        String dsrId = map.get("dsrId").toString();
        String printDate = map.get("printDate").toString(); // 年月日
        String cfAddress = map.get("cfAddress").toString();
        String cfXw = map.get("cfXw").toString(); // 处罚行为
        String wfYj = map.get("wfYj").toString(); // 违法依据
        String cfYj = map.get("cfYj").toString(); // 处罚依据
        String cfJe = map.get("cfJe").toString(); //   处罚金额
        String zfrName = map.get("zfrName").toString(); // 执法人员name
        String zfrId = map.get("zfrId").toString(); // 执法人员 loginId

        String test1 = "   现查明，当事人于" + printDate + "在" + cfAddress + "进行" + cfXw + "的行为。该行为违反了" + wfYj +"的规定，现责令你立即改正，并依据"+cfYj+"的规定，决定给予你"+cfJe+"元罚款的行政处罚。";
        String test2 = "   逾期不缴纳罚款的，每日按罚款数额的3%加处罚额。";
        String test3 = "   如不服本决定者，可在接到本决定书之日起60日内向张家港市人民政府或向苏州市城市管理行政局申请复议，也可在接到本决定书之日起3个月内向人民法院提起诉讼。逾期不申请复议，也不向人民法院起诉，又不履行本决定的，本机关将申请人民法院强制执行。复议或诉讼期间，本决定不停止执行。";
        String test4 = "   执法人员:" + zfrName + " " + zfrId + "处罚地点："+ cfAddress;
        String test5 = "";
        // 标题
        iPrinter.pageSetup(580, 1100);
        iPrinter.drawText(160 , 0 , "张家港市城市管理行政局" , 2 , 0 , 1 , false , false);
        iPrinter.drawText(140 , 42 , " 行  政  处  罚  决  定  书" , 2 , 0, 1,false , false);
        iPrinter.drawText(330 , 42 + 42 , "No:" + cfNo , 2, 0,1,false,false);
        iPrinter.drawText(20 ,  42 + 42 + 42 , "公民：" + dsrPerson + "  身份证号码："+ dsrId , 2, 0 , 1, false , false);
        int linCount1  = LableTest(iPrinter, 20,  4*42 , 2,     test1);
        int linCount2  = LableTest(iPrinter , 20 , 42*4  + (linCount1 + 2) * 32  , 2 , test2 );
        int linCount3 = LableTest(iPrinter , 20 , 42*4 + (linCount1 + linCount2 + 4 )*32 , 2 , test3);
        int linCount4 = LableTest(iPrinter,20 , 42*4 + (linCount1 + linCount2 + linCount3 + 6)*32 , 2 , test4);
        iPrinter.drawText(180 ,  42*4 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 ,"张家港市城市管理行政执法局(印章)",2,0,1,false,false);
        iPrinter.drawText(20 , 42*6 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 , "当事人：", 2,0,1,false , false);
        iPrinter.drawText(20 , 42*7 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 , "日期：      年      月     日" , 2,0,1,false,false);
        iPrinter.drawText(160 , 42*9 + (linCount1 + linCount2 + linCount3 + linCount4 + 8)*32 , "（第二联   当事人执收）" , 2, 0 , 1,false ,false);
        iPrinter.print(0, 0);
    }


}
