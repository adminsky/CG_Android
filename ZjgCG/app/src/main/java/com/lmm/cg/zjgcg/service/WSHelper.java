package com.lmm.cg.zjgcg.service;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;


public class WSHelper {
	final static String WSUrl = "http://221.224.118.58:8080/zjgszcg/WebServiceSzcg/QWGL.asmx";
	private static String namespace = "http://tempuri.org/";

	/*************************************
	 * 获取web services内容
	 * 
	 * @param url
	 * @param params
	 * @return
	 *************************************/

    //3.0以上版本限制访问网络，每次访问网络前需要重写方法。
    /*private static void ConnectToWeb() {             
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()                   
            .detectDiskReads()                   
            .detectDiskWrites()                   
            .detectNetwork()   // or .detectAll() for all detectable problems                   
            .penaltyLog()                   
            .build());           
             StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()                   
            .detectLeakedSqlLiteObjects()                   
            .penaltyLog()                   
            .penaltyDeath()                  
            .build()); 
    }
	*/
	 
	//十六进制下数字到字符的映射数组
	 private final static String[] hexDigits = { "0", "1", "2", "3", "4", "5","6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };
	
	  
	/**
	  * 
	  * Creation  Date:2008-3-14
	  * 转换字节数组为十六进制字符串
	  * @param b  字节数组
	  * @return String 十六进制字符串
	  * @Author tianw
	  */
	 private static String byteArrayToHexString(byte[] b) {
	  StringBuffer resultSb = new StringBuffer();
	  for (int i = 0; i < b.length; i++) {
	   
	   String ss = byteToHexString(b[i]);   
	   resultSb.append(ss);
	  }
	  
	  return resultSb.toString();
	 }
	 /** 将一个字节转化成十六进制形式的字符串 */
	 private static String byteToHexString(byte b) {
	  int n = b;
	  if (n < 0)
	   n = 256 + n;
	  int d1 = n / 16;
	  int d2 = n % 16;
	  return hexDigits[d1] + hexDigits[d2];
	 }
	
	public static String MD5(String s){
		MessageDigest  md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
        md.update(s.getBytes());
        return byteArrayToHexString(md.digest());
}
	
public static String md5Three(String clientId,String pwd,String timestamp){
		clientId=clientId==null?"":clientId;
		pwd=pwd==null?"":pwd;
		timestamp=timestamp==null?"":timestamp;
		while(timestamp.length()<10){
			timestamp="0"+timestamp;
		}
		MessageDigest  md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
        md.update(clientId.getBytes());
        md.update(new byte[7]);
        md.update(pwd.getBytes());
        md.update(timestamp.getBytes());
        
        return byteArrayToHexString(md.digest());        
}

	
	public static String GetResponse(String url, String method,
			List<BasicNameValuePair> params) {
		try {
			SoapObject request = new SoapObject(namespace, method);
			for (int i = 0, len = params.size(); i < len; i++) {
				request.addProperty(params.get(i).getName(), params.get(i)
						.getValue());
			}
			SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
					SoapEnvelope.VER11);
			envelope.dotNet = true;
			envelope.setOutputSoapObject(request);
			// 设置30秒超时
			MyAndroidHttpTransport transport = new MyAndroidHttpTransport(url,
					30000);
			transport.call(namespace + method, envelope);

			SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
			return result.toString();
		} catch (Exception e) {
			return "Error: Call the Service error!" + e.getMessage();
		}
	}
}